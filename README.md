![yo](https://img.shields.io/badge/license-unlicense%20-green) ![yoy](https://img.shields.io/github/languages/top/caeden045/Ultimate-Calculator) ![rada](https://img.shields.io/github/repo-size/caeden045/Ultimate-Calculator) ![lines](https://img.shields.io/tokei/lines/github/caeden045/Ultimate-Calculator) ![maintain](https://img.shields.io/maintenance/yes/2021) ![commit](https://img.shields.io/github/commit-activity/m/caeden045/Ultimate-Calculator) ![com2](https://img.shields.io/github/commits-since/caeden045/Ultimate-Calculator/2.3.0) ![uwu](https://img.shields.io/github/last-commit/caeden045/Ultimate-Calculator)
# Ultimate-Calculator

A handy, totally useful Python calculator




more specifically...
A Python based calculator written upon my earlier created shrek calculator which was based upon a meme, the premise being that the day could be measured by how many times you could watch shrek in 24 hours. each time passing equivalent to the movie's length would be a "shrek" and would be counted like hours. (Around 18 shreks fit in a day). From there I expanded to add a fly to horsepower converter and a few other extremely useful programs. Thennnn I deleted all of it on accident and started over. So here we are!

This program is still very rough around the edges and I am still trying to get a good grip on Python as a whole. Please feel free to suggest bug fixes or new features as desired!

# Initial Screen
![geg](https://user-images.githubusercontent.com/84045381/131939608-f202f3af-0cc2-477c-8f17-04e3db09c691.PNG)



Pretty, right?
This program currently has an executable available, however the most up to date will be from the main uc2.py file.
